<?php
return [
    'core' =>
        [
        'root' => $_SERVER['DOCUMENT_ROOT'],
        'debug' => true,
        '404' => '/404.php',
        ],
    'db' =>
        [
            'host' => 'localhost',
            'dbname' => 'task',
            'user' => 'root',
            'pass' => '1101',
        ],
    'session' =>
        [
            'name' => 'session_id',
            'lifetime' => 30 * 60,
            'hash' => 'sha256',
        ],
    'cookie' =>
        [
            'login' =>
                [
                    'lifetime' => 50 * 24 * 3600,
                ],
        ],
    'config' =>
        [
            'read_only' => true,
        ],
];