<?php

namespace Task\Core\Controller;

use Task\Core\DB\Post\Message;
use Task\Core\DB\User;

class PostMessage
{
    public function add(int $userID): bool
    {
        if (empty($form)) {return false;}
        $form['author_id']=$userID;

        return (new Message())->_addMessageAsArray($form);
    }

    public function getNewPostList(int $userID)
    {
        $Message = new Message();
        $res = $Message->_getListMessageTitleFromDB($userID);

        if (!$res->getRowCount()){
            return [];
        }

        return $res->getAll();
    }

    public function checkDetailPageQuery(){
        if (isset($_REQUEST['id'])&&intval($_REQUEST['id'])){
            $messageID = intval($_REQUEST['id']);
            $Message = new Message();
            $res = $Message->_getMessageFromDB($messageID);

            if (!$res->getRowCount()){
                return false;
            }

            $Message->_setReadingFlagForMessageFromDB($messageID);

            $message = $res->getNext();

            //добавляем инфо об авторе
            $User = new User();
            $user = $User->getUsersByUIDs([$message['author_id']])->getNext();
            $message['author']['last_name'] = $user['last_name'];
            $message['author']['first_name'] = $user['first_name'];
            $message['author']['middle_name'] = $user['middle_name'];
            $message['author']['email'] = $user['email'];

            return $message;
        }

        return false;
    }

}