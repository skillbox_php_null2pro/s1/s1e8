<?php

namespace Task\Core;

use Task\Core;

abstract class Component
{
    protected $template;
    protected $settings;
    protected $input;
    protected $result;

    abstract protected function exec();

    protected function includeTemplate()
    {
        /**@var $CORE Core */
        $CORE = Register::get('core');

        $templatePath = $this->getTemplatePath();

        $SETTINGS = $this->settings;
        $RESULT = $this->result;

        require $templatePath;
    }

    private function getTemplatePath()
    {
        /**@var $CORE Core */
        $CORE = Register::get('core');
        $arNamespace = explode('\\', get_class($this));
        $class = strtolower(array_pop($arNamespace));
        return $CORE->Site->getTemplate()->getRootAbs() . '/components/' . $class . '/template/' . $this->template . '/template.php';
    }
}