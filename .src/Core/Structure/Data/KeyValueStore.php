<?php

namespace Task\Core\Structure\Data;

class KeyValueStore
{
    protected $store = [];

    public function set($key, $value): void
    {
        $this->store[$key] = $value;
    }

    public function unset($key): void
    {
        unset($this->store[$key]);
    }

    public function get($key)
    {
        return @$this->store[$key];
    }

    public function list(): array
    {
        return $this->store;
    }
}