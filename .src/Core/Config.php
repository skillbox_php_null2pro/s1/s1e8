<?php

namespace Task\Core;

use Task\Core\Structure\Singleton;

class Config extends Singleton
{
    private $config;
    private $section;
    private $path;

    protected function __construct()
    {
        $this->path = $_SERVER['DOCUMENT_ROOT'] . '/.config.php';
        $this->config = require $this->path;

        //обьявляем константы
        $define = $this->getSection('define');
        if ($define) {
            foreach ($define as $key => $value) {
                if (!defined($key)) {
                    define($key, $value);
                }
            }
        }
    }

    public function __destruct()
    {
        if (!$this->getKey('read_only', 'config')) {
            $content = '<?php' . PHP_EOL . 'return ' . var_export($this->config, true) . ';';
            if (!file_exists($this->path) || md5_file($this->path) != md5($content)) {
                file_put_contents($this->path, $content);
            }
        }
    }

    public function getSection($name)
    {
        if (isset($this->config[$name])) {
            $this->section = $name;
            return $this->config[$this->section];
        }

        return false;
    }

    public function getKey($name, $section = false)
    {
        if ($section === false) {
            $section = $this->section;
        }

        if (isset($this->config[$section][$name])) {
            return $this->config[$section][$name];
        }

        return false;
    }

    public function newSection($name): bool
    {
        if (!isset($this->config[$name])) {
            $this->section = $name;
            $this->config[$this->section] = array();
            return true;
        }

        return false;
    }

    public function newKey($name, $value, $section = false): bool
    {
        if ($section === false) {
            $section = $this->section;
        }

        if (!isset($this->config[$section][$name])) {
            $this->config[$section][$name] = $value;
            return true;
        }

        return false;
    }

    public function updateSection($name, array $value): bool
    {
        if (isset($this->config[$name])) {
            $this->section = $name;
            $this->config[$this->section] = $value;
            return true;
        }

        return false;
    }

    public function updateKey($name, $value, $section = false): bool
    {
        if ($section === false) {
            $section = $this->section;
        }

        if (isset($this->config[$section][$name])) {
            $this->config[$section][$name] = $value;
            return true;
        }

        return false;
    }

}