<?php

namespace Task\Core;

use Task\Core\Site\Page;
use Task\Core\Site\Template;
use Task\Core\Structure\Singleton;

class Site extends Singleton
{
    private $root;

    /**@var Template */
    private $Template;

    /**@var Page */
    private $Page;

    protected function __construct()
    {
        $this->root = Config::getInstance()->getKey('root', 'core');
        $this->Template = new Template();
        $this->Page = new Page($this->root . Request::getInstance()->getFile());
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->Template;
    }

    /**
     * @return mixed
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @return Page
     */
    public function getPage(): Page
    {
        return $this->Page;
    }


}