<?php

namespace Task\Core\DB;

class Group extends Table
{
    public function getGroupByGID(int $gid): Result
    {
        $sql = 'select * FROM c_auth_group where id=:gid';
        $psql = $this->pdo->prepare($sql);
        $psql->bindParam(':gid', $gid);
        $psql->execute();

        return new Result($psql);
    }

    public function getList(): Result
    {
        $sql = 'select * FROM c_auth_group';
        $psql = $this->pdo->prepare($sql);
        $psql->execute();

        return new Result($psql);
    }

    public function getPostRightsByGIDs(array $arGIDs)
    {
        $sql = 'select post FROM c_auth_group where id in (' . implode(',', array_fill(0, count($arGIDs), '?')) . ')';
        $psql = $this->pdo->prepare($sql);
        $psql->execute($arGIDs);

        return new Result($psql);
    }
}