<?php

namespace Task\Core\DB;

class Result
{
    /**@var \PDOStatement */
    protected $pdoStatement;

    public function __construct(\PDOStatement $statement)
    {
        $this->pdoStatement = $statement;
    }

    public function getRowCount()
    {
        return $this->pdoStatement->rowCount();
    }

    public function getAll()
    {
        return $this->pdoStatement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getNext()
    {
        return $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);
    }

    public function getNextClass(string $className){
        $this->pdoStatement->setFetchMode(\PDO::FETCH_CLASS, $className);
        return $this->pdoStatement->fetch();
    }

    public function getError()
    {
        return [$this->pdoStatement->errorCode() => $this->pdoStatement->errorInfo()];
    }
}