<?php

namespace Task\Core\DB;

use Task\Core\DB;

class Table
{
    /**@var \PDO */
    protected $pdo;

    /**@var DB */
    protected $DB;

    public function __construct()
    {
        $this->pdo = DB::getInstance()->getPdo();
        $this->DB = DB::getInstance();
    }

}