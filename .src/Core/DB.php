<?php

namespace Task\Core;

use \PDO;
use \PDOException;

class DB
{
    private static $instance;
    /**@var \PDO */
    private $pdo;

    private function __clone() { }

    private function __wakeup() { }

    private function __construct()
    {
        $Config = Config::getInstance();
        $this->connect($Config->getSection('db'));
    }

    public function __destruct()
    {
        //откатываем некорректно закрытые транзакции
        if ($this->pdo->inTransaction()) {
            $this->pdo->rollBack();
        }
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new DB();
        }

        return self::$instance;
    }

    public function getPdo()
    {
        return $this->pdo;
    }

    public function startTransaction(): bool
    {
        return $this->pdo->beginTransaction();
    }

    /**
     * Завершает транзакцию
     * @param bool $error - если true, откатывает изменения
     * @return bool - результат операции
     */
    public function endTransaction(bool $error = false): bool
    {
        //откатываем транзакции при ошибках
        if ($error) {
            return $this->pdo->rollBack();
        }

        return $this->pdo->commit();
    }

    public function getLastInsertedID(){
        return $this->pdo->lastInsertId();
    }

    private function connect(array $config)
    {
        if (!is_null($this->pdo)) {
            return;
        }

        try {
            $this->pdo = new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'], $config['user'], $config['pass']);
        } catch (PDOException $e) {
            echo '<pre>';
            echo 'Нет доступа к базе данных: ' . $e->getMessage();
            die();
        }
    }
}