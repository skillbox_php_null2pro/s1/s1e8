<?php

namespace Task\Core;

class Router
{
    /**
     * Возвращает путь к файлу запрошенного клиентом ресурса или к 404
     * @return string
     */
    public function route(): string
    {
        $root = Config::getInstance()->getKey('root', 'core');

        $url_rewrite = include $root . '/.url_rewrite.php';

        if ($url_rewrite === false) {
            die('Критическая ошибка! </br> Нет доступа к файлу конфигурации роутера </br> Проверьте наличие и права .url_rewrite.php');
        }

        //404
        if ($url_rewrite === 1 || !file_exists($root . $url_rewrite)) {
            return Config::getInstance()->getKey('404','core');
        }

        return $url_rewrite;
    }
}