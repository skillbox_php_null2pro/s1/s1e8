<?php

namespace Task\Core;


class Cache
{
    private $cacheTime;
    private $cacheId;
    private $path;

    public function __construct($cacheId, $path='') {
        $this->cacheId=md5(serialize($cacheId));
        $this->path=
    }

    public function file_get_contents(string $filename, bool $use_include_path = false, $context = null, $offset = 0, $maxlen = null){
       return file_get_contents($filename, $use_include_path, $context, $offset, $maxlen);
    }

    private function isCached($filename){
        return file_exists($filename) && filemtime($filename) <= (time() - $this->cacheTime);
    }

}