<?php

namespace Task\Core;

use Task\Core\Structure\Singleton;

class Request extends Singleton
{
    private $uri;
    private $query;
    private $file;

    protected function __construct()
    {
        $this->file = (new Router())->route(); //производим роутинг по белому списку
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->query = $_SERVER['QUERY_STRING'];
    }

    /**
     * Возвращает REQUEST_URI запроса
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * Возвращает QUERY_STRING запроса
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * Возвращает путь к файлу запрошенного клиентом ресурса
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }
}