<?php

namespace Task\Core\Site;

class Template
{
    private $name;
    private $root;

    public function __construct(string $templateName = 'default')
    {
        $this->name = $templateName;
        $this->root = '/include/templates/' . $this->name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getRoot(): string
    {
        return $this->root;
    }
}