<?php

namespace Task\Core\Site;


use Task\Core;

class Page
{
    private $path;

    public function __construct(string $path)
    {
        $this->path = $path;
        $this->execPage();
    }

    public function addCss($fileName)
    {
        $html = '<link href="#path#" rel="stylesheet" />';
        $this->injectHtml($html, 'css', $fileName);
    }

    public function addJs($fileName)
    {
        $html = '<script src="#path#"></script>';
        $this->injectHtml($html, 'js', $fileName);
    }

    private function injectHtml($html, $section, $fileName)
    {
        /**@var $CORE Core */
        $CORE = Core::getInstance();

        $path = $CORE->Site->getRoot();
        $path .= $CORE->Site->getTemplate()->getRoot();
        $path .= '/res/' . $section . '/' . $fileName;

        if (file_exists($path)) {
            $path .= '?' . md5_file($path);
            echo str_replace('#path#', $path, $html);
        }
    }

    private function execPage()
    {
        require $this->path;
    }
}