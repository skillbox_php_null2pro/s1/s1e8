<?php

namespace Task;


use Task\Core\Config;
use Task\Core\Request;
use Task\Core\Session;
use Task\Core\Site;
use Task\Core\Structure\Data\KeyValueStore;
use Task\Core\Structure\Singleton;

class Core extends Singleton
{
    /**@var Config */
    public $Config;

    /**@var Site */
    public $Site;

    /**@var Session */
    public $Session;

    /**@var Cookie */
    public $Cookie;

    /**@var DB */
    public $DB;

    /**@var User */
    public $User;

    /**@var KeyValueStore */
    public $DataStore;

    /**@var Request */
    public $Request;

    protected function __construct()
    {
        echo '1';
        $this->Config = Config::getInstance();
        $this->Request = Request::getInstance();
        $this->DataStore = new KeyValueStore();
        $this->Site = Site::getInstance();
        //$this->Session = new Session();
        //        $this->Cookie = new Cookie('login');
        //        $this->DB = DB::getInstance();
        //        $this->User = new User();

        //        $this->exec();
    }

    private function exec()
    {
        //        $this->setUserLogin();
        //        $this->logout();
        //        $this->auth();
        //        $this->postAuth();
    }

    //    private function setUserLogin()
    //    {
    //        if (!empty($_SESSION['login'])) {
    //            $this->User = new User($_SESSION['login']);
    //        } elseif (!empty($this->Cookie->getValue())) {
    //            $this->User = new User($this->Cookie->getValue());
    //        }
    //    }
    //
    //    private function logout()
    //    {
    //        if (isset($_GET['logout'])) {
    //            $this->Session->destroy();
    //            Route::go();
    //        }
    //    }
    //
    //    private function auth()
    //    {
    //        if (isset($_POST['action'])) {
    //            switch ($_POST['action']) {
    //                case 'Войти':
    //
    //                    if ($this->Session->isAuth()) {
    //                        break;
    //                    }
    //
    //                    $login = $_POST['login'];
    //                    $password = $_POST['password'];
    //                    if (empty($login) || empty($password)) {
    //                        break;
    //                    }
    //
    //                    $login = htmlspecialchars(trim($login));
    //                    $password = htmlspecialchars(trim($password));
    //
    //                    $this->Cookie->setValue($login);
    //
    //                    $auth = $this->User->authByLogin($login, $password); //Авторизация по БД
    //
    //                    $this->Session->setAuth($auth);
    //
    //                    if ($this->Session->isAuth()) {
    //                        $_SESSION['login'] = $login;
    //                        Route::go();
    //                    }
    //
    //                    break;
    //
    //                case 'Выйти':
    //
    //                    $this->deAuth();
    //
    //                    break;
    //            }
    //        }
    //    }
    //
    //    private function postAuth()
    //    {
    //        if ($this->Session->isAuth()) {
    //            $this->Session->refresh();
    //            $this->Cookie->refresh();
    //            $this->User->fillUserInfo();
    //        } elseif (parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) != '/') {
    //            $this->Session->destroy();
    //            Route::go();
    //        }
    //    }
    //
    //    private function deAuth()
    //    {
    //        $this->Session->setAuth(false);
    //        Route::go();
    //    }


}