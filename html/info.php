<?
/**@var $this \Task\Core\Site\Page*/

$baseURL = 'http://geohelper.info/api/v1/';
$apiKey = 'apiKey=0yOtEW0vyAxBuVPlxfczictoISDsF2IR';
$baseOpts = 'locale[lang]=ru&locale[fallbackLang]=en';

$cachePath = $_SERVER['DOCUMENT_ROOT'] . '/.cache/geohelper/country_list.json';
$cacheTime = 24 * 3600;
$isCached = file_exists($cachePath) && filemtime($cachePath) <= (time() - $cacheTime);

$url = $baseURL . 'countries' . '?' . $apiKey . '&' . $baseOpts;

if ($isCached) {
    $url = $cachePath;
}

$json = file_get_contents($url);

if (!$isCached) {
    mkdir(dirname($cachePath),0777,true);
    file_put_contents($cachePath, $json);
}

$arCountries = json_decode($json, true);
unset($json);

if (isset($_REQUEST['ajax'])){
    $result=null;

    if (!empty($_REQUEST['country'])){
        $countryIdx = array_column($arCountries['result'], 'iso');
        $idx = array_search($_REQUEST['country'], $countryIdx);

        if ($idx!==false){
            $baseOpts = str_replace('locale[lang]=ru', 'locale[lang]='.$arCountries[$idx]['languages']['0'], $baseOpts);
        }

        $url = $baseURL.'cities'.'?'.$apiKey.'&'.$baseOpts.'&filter[countryIso]=' . trim($_REQUEST['country']);

        if (!empty($_REQUEST['city'])){
            $url.='&filter[name]='.trim($_REQUEST['city']);
        }

        $result = file_get_contents($url);
    }

    $result = json_decode($result);
    $result = [$countryIdx,$result];
    $result = json_encode($result);
    die($result);
}


//echo '<pre>';
//die(var_export($arCountries, true));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Личная информация</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <?php $this->addCss('styles.css')?>
    <?php $this->addCss('jquery.fancybox.css')?>
    <?php $this->addCss('jquery-ui.css')?>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="/include/templates/default/res/js/jquery.tablednd_0_5.js"></script>
    <script src="/include/templates/default/res/js/iphone-style-checkboxes.js" type="text/javascript"></script>
    <script type="text/javascript" src="/include/templates/default/res/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/include/templates/default/res/js/jquery.maskedinput.js"></script>
    <script type="text/javascript" src="/include/templates/default/res/js/jquery-ui.min.js"></script>

    <!-- Скрипт для списка папок -->
    <script>
        $(document).ready(function () {
            $('.fo-title').append('<span class="acc"></span>');
            $('.fo-post span.acc').each(function () {
                var trigger = $(this),
                    state = false,
                    el = trigger.parent().next('.fo-entry');
                trigger.click(function () {
                    state = !state;
                    el.slideToggle();
                    trigger.parent().parent().toggleClass('fo-inactive');
                });
            });
            $(".popup").fancybox({
                padding: 0,
                width: 640,
                height: 'auto',
                autoSize: false,
                closeBtn: false
            });

            $('.folder-users-list :checkbox').change(function () {
                console.log('hello');
                console.log($(this).find(':checkbox').val());
            });
        });
    </script>
    <!-- Скрипт для перетаскивания -->
    <script>
        $(document).ready(function () {
            // Initialise the table
            $("#table-2").tableDnD({
                onDragClass: "myDragClass",
                dragHandle: "dragHandle"
            });

            $("#table-2 > tbody > tr").hover(function () {
                $(this.cells[0]).addClass('showDragHandle');
            }, function () {
                $(this.cells[0]).removeClass('showDragHandle');
            });

        });
    </script>

    <!-- Скрипт для чекбоксов слайдеров -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('.iphone-checkbox').iphoneStyle();
        });
    </script>

    <!-- phone -->
    <script type="text/javascript">
        $(function () {
            $('input[name="info[phone]"]').mask('+7 (999) 999-99-99');
        });
    </script>

</head>

<body>

<div id="opaco" class="hidden"></div>
<div id="popup" class="hidden"></div>

<div class="header">
    <div class="logo">TaskManager</div>
    <div class="menu">
        <a href="#">Списки</a>
    </div>
    <div class="settings">Стас Морозов (<a href="#"><img src="/include/templates/default/res/img/i/setting-top-icon.gif"/></a>) &mdash;
        <a href="#">Выход</a>
    </div>
    <div style="clear: both;"></div>
</div>

<div class="contif">
    <div class="cshadow">
        <form class="info" action="" method="post">
            <div class="paddingtwe">

                <table>
                    <tr>
                        <td class="frmopt">
                            <div class="info-title">
                                <h1>Личная информация</h1>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt">
                            <div class="info-photo">
                                <div class="big-photo-info">
                                    <img src="/include/templates/default/res/img/i/no-photo-big.png"/>
                                </div>
                                <div class="photo-info-set">
                                    <a href="#">Удалить фото</a> или
                                    <a href="#">загрузить другое</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt">
                            <div class="inpsty">
                                <input type="text" name="info[first_name]" placeholder="Имя" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt">
                            <div class="inpsty err">
                                <input type="text" name="info[last_name]" placeholder="Фамилия" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt">
                            <div class="inpsty">
                                <input type="text" name="info[middle_name]" placeholder="Отчество" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt">
                            <div class="inpsty">
                                <input type="email" name="info[email]" placeholder="E-mail" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt doppadding">
                            <div class="inpsty">
                                <input type="password" name="info[password]" placeholder="Новый пароль">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt">
                            <div class="inpsty">
                                <select name="info[country]" required>
                                    <option disabled selected>Страна</option>
                                    <?if(isset($arCountries['success'])&&$arCountries['success']===true):?>
                                    <?foreach ($arCountries['result'] as $country):?>
                                    <option value="<?=$country['iso']?>"><?=$country['name']?></option>
                                    <?endforeach;?>
                                    <?endif;?>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt">
                            <div class="inpsty">
                                <input name="info[city]" required>
                            </div>
                        </td>
                    </tr>
                    <script>
                        var cityAjax = false;
                        $(function () {
                            $('select[name="info[country]"]').change(function (e) {
                                console.dir($('select[name="info[country]"]').val());
                            });
                            $('input[name="info[city]"]').keyup(function (e) {
                                //if (e.keyCode==8){
                                $('input[name="info[city]"]').autocomplete('close');
                                //}
                            });
                            $('input[name="info[city]"]').autocomplete({
                                source: function (input, callback) {
                                    if (!cityAjax) {
                                        cityAjax = true;
                                        $.ajax({
                                            data: {
                                                ajax: true,
                                                country: $('select[name="info[country]"]').val(),
                                                city: input.term
                                            },
                                            dataType: "json",
                                            timeout: 1000,
                                            error: function () {
                                                cityAjax = false;
                                            },
                                            success: function (data) {
                                                console.dir(data);
                                                if (data.success !== undefined && data.success === true) {
                                                    var result = [];
                                                    data.result.forEach(function (obj, i, arr) {
                                                        let name = '';
                                                        if (obj.localityType !== undefined && obj.localityType.name !== undefined) {
                                                            name = obj.localityType.name + ' ';
                                                        }
                                                        name += obj.name;
                                                        if (obj.area !== undefined) {
                                                            name += ' (' + obj.area + ' р-н)'
                                                        }
                                                        result.push(name);
                                                    });
                                                    if (data.pagination.totalCount - result.length > 0) {
                                                        result.push('...найдено ещё ' + (data.pagination.totalCount - result.length) + ' объектов');
                                                    }
                                                    //console.dir(result);
                                                    callback(result);
                                                }
                                                cityAjax = false;
                                            }
                                        });

                                    }
                                }
                            });
                        });
                    </script>
                    <tr>
                        <td class="frmopt">
                            <div class="inpsty">
                                <input type="text" name="info[phone]" placeholder="Телефон (Моб.)">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt doppadding">
                            <div class="yellow-alert" style="width: 250px;">
                                <label><input type="checkbox" name="info[spam]" checked> Уведомления на e-mail</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="frmopt">
                            <div class="save-bt">
                                <input type="submit" value="Сохранить">
                            </div>
                            <div class="cancel-bt">или
                                <a href="#">Отменить</a>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="err-form">В поле &laquo;Фамилия&raquo;: Нельзя использовать пробел</div>

            </div>
        </form>
    </div>

    <div class="footer">&copy;&nbsp;<nobr>2018</nobr>
        task.manager (<a href="#">support@task.manager</a>).
    </div>

</div>
<pre style="position: absolute;top: 10%;z-index: -1;">
    <?=var_export($arCountries)?>
</pre>
</body>
</html>