<ul>
    <?php foreach ($RESULT['menu'] as $item):?>
        <li>
            <a href="<?=$item['path']?>" style="text-decoration: <?= ($item['selected']) ? 'underline' : 'none' ?>;">
                <?=\Task\Helper\General::titleCrop($item['title'])?>
            </a>
        </li>
    <?php endforeach?>
</ul>