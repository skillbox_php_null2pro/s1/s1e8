Сущности:
* Пользователь
  * содержит данные пользователя:
    * id(int11 PK AI)
    * foto(varchar255 null) - хэш файла
    * фамилия*(varchar255 NN)
    * имя*(varchar255 NN)
    * отчество*(varchar255 NN)
    * email*(varchar255 NN)
    * password*(varchar255 NN)
    * phone*(varchar255 NN)
    * spam(bit1 1) - флаг
    * country*(varchar255 NN)
    * city*(varchar255 NN)
* Группа пользователей
  * содержит права
* Список задач
  * содержит описание и настройки
* Запись
 * id
 * text
 * 
