<?php
//этот ЧПУ-скрипт должен любым способом обработать $_SERVER и вернуть относительный url к реальному файлу
$request = explode('/', $_SERVER['REQUEST_URI']);

//ajax
if ($request[1]==='ajax'){
    return 'ajax';
}

//info
if ($request[1]==='info'){
    return '/html/info.php';
}

//debug
if (\Task\Core\Config::getInstance()->getKey('debug','core')) {
    \Task\Helper\General::debug($request);
}